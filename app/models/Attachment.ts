
import { Table, Column, DataType, BelongsTo, ForeignKey } from "sequelize-typescript";
import { BaseModel } from "../libraries/BaseModel";
import { Justificante } from "./Justificante";
import { User } from "./User";

@Table
export class Attachment extends BaseModel<Attachment> {

  @ForeignKey(() => User)
  @Column
  user_id: number;

  @BelongsTo(() => User)
  user: User;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    defaultValue: "default"
  })
  file_name: string;

  @ForeignKey(() => Justificante)
  @Column
  just_id: number;

  @BelongsTo(() => Justificante)
  justificante: Justificante;

}
