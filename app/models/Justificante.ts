
import { Table, Column, DataType, BelongsTo, ForeignKey, HasMany } from "sequelize-typescript";
import { BaseModel } from "../libraries/BaseModel";
import { User } from "./User";
import { Attachment } from "./Attachment";

@Table
export class Justificante extends BaseModel<Justificante> {

  @ForeignKey(() => User)
  @Column
  user_id: number;

  @BelongsTo(() => User)
  user: User;

  @Column({
    type: DataType.DATE
  })
  date_1: Date;

  @Column({
    type: DataType.DATE
  })
  date_2: Date;

  @Column({
    type: DataType.ENUM("Médico", "Personal", "Otro"),
    allowNull: false,
    defaultValue: "Personal"
  })
  type: "Médico" | "Personal" | "Otro";

  @Column({
    type: DataType.STRING,
    allowNull: true,
    defaultValue: null
  })
  explanatory: string;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: true,
    defaultValue: null
  })
  approved: boolean;

  @HasMany(() => Attachment)
  attachments: Attachment[];

}
