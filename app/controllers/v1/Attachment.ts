
import { Controller } from "../../libraries/Controller";
import { Attachment } from "../../models/Attachment";
import { Request, Response, Router } from "express";
import { validateJWT, filterOwner, appendUser, stripNestedObjects, filterRoles } from "../../policies/General";
import { isNullOrUndefined } from "util";
import { log } from "../../libraries/Log";

var express = require('express')
var multer = require('multer')
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'upload/')
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.jpg`)
  }
})

var upload = multer({ storage: storage })

var app = express()

export class AttachmentController extends Controller {


  constructor() {
    super();
    this.name = "attachment";
    this.model = Attachment;
  }

  routes(): Router {

    this.router.get("/", validateJWT("access"), filterOwner(), (req, res) => this.serveAll(req, res));
    this.router.get("/:id", validateJWT("access"), filterOwner(), (req, res) => this.serveOne(req, res));

    this.router.post("/upload", upload.single('image'), (req, res) => this.fileUpload(req, res));
    this.router.post("/", validateJWT("access"), stripNestedObjects(), filterOwner(), appendUser(), (req, res) => this.create(req, res));

    this.router.put("/:id", validateJWT("access"), stripNestedObjects(), filterRoles(["admin"]), (req, res) => this.update(req, res));
    this.router.delete("/:id", validateJWT("access"), filterOwner(), (req, res) => this.destroy(req, res));

    return this.router;
  }

  fileUpload(req, res: Response) {
    res.set("Content-Count", String('Perfecto'));
    res.status(200).json(req.file.filename);
    return null;
  }


  serveAll(req: Request, res: Response) {
    this.model
      .findAll({
        where: this.parseWhere(req),
        include: this.parseInclude(req)
      })
      .then(results => {
        var path = require('path');
        results.map((result) => {
          // res.set("Content-Count", String(result.count));
          // res.status(200).json(result.rows);
          // res.status(200).json(`${req.baseUrl}/upload/${result.file_name}`);
          res.sendFile(path.resolve(`upload/${result.file_name}`));
        })
        return null;
      })
      .catch(err => {
        if (err) Controller.serverError(res, err);
      });
  }

  serveOne(req: Request, res: Response) {
    // For applying constraints (usefull with policies)
    let where = this.parseWhere(req);
    where.id = req.params.id;
    this.model
      .findOne({
        where: where,
        include: this.parseInclude(req)
      })
      .then(result => {
        if (!result) res.status(404).end();
        else {
          var path = require('path');
          res.sendFile(path.resolve(`upload/${result.file_name}`));
          // res.status(200).json(result);
        }
        return null;
      })
      .catch(err => {
        if (err) Controller.serverError(res, err);
      });
  }

}

const attachment = new AttachmentController();
export default attachment;