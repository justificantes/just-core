
import { Controller } from "./../../libraries/Controller";
import { Justificante } from "./../../models/Justificante";
import { Request, Response, Router } from "express";
import { validateJWT, filterOwner, appendUser, stripNestedObjects, filterRoles } from "./../../policies/General";

const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

export class JustificanteController extends Controller {

  constructor() {
    super();
    this.name = "justificante";
    this.model = Justificante;
  }

  routes(): Router {

    this.router.get("/", validateJWT("access"), filterOwner(), (req, res) => this.find(req, res));
    this.router.get("/admin", validateJWT("access"), filterRoles(["admin"]), (req, res) => this.find(req, res));
    this.router.get("/:id", validateJWT("access"), filterOwner(), (req, res) => this.findOne(req, res));
    this.router.post("/", validateJWT("access"), stripNestedObjects(), filterOwner(), appendUser(), (req, res) => this.create(req, res));
    this.router.put("/:id", validateJWT("access"), stripNestedObjects(), filterRoles(["admin"]), (req, res) => this.update(req, res));
    this.router.delete("/:id", validateJWT("access"), filterOwner(), (req, res) => this.destroy(req, res));

    return this.router;
  }

}

const justificante = new JustificanteController();
export default justificante;