'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Justificante', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'User',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        foreignKey: true,
      },
      date_1: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      date_2: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      type: {
        type: Sequelize.ENUM("Médico", "Personal", "Otro"),
        allowNull: false,
        defaultValue: "Personal"
      },
      explanatory: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null
      },
      approved: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: null
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      deleted_at: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    }, {
      charset: 'utf8'
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Justificante');

  }
};
